# web-kelompok-3

Repository Project Laravel IM 23 Sanbercode. Kelompok 3 <br>
<h3> SanberHub : Social Media Coder Santai dan Berkualitas.</h3>
<hr>
<h4> Member : </h4>
<ul>
<li> Raxel Adam Kamisik </li>
</ul>

<em>Last Updated : [20/04/2021 22:57]</em>

<em> Link Video Demo (Display only) update versi [19/04/2021 23:56] :</em>
https://drive.google.com/file/d/1S_43wLNH7p_a_Kc4LIY9XLMYVGmGXhmH/view?usp=sharing

<em> Link Video Demo versi [17/04/2021 5:40] :</em>
https://drive.google.com/file/d/1qSbshknXpGSwqEugSVZi6peVy3WSMXmN/view?usp=sharing
<hr>

<strong>Petunjuk penggunaan saat clone/pengerjaan :</strong>

0a. Lakukan Git Clone terlebih dahulu, setelah itu jangan lupa membuat branch dengan "git branch nama_branch" lalu "git checkout nama_branch". Jika sudah yakin bisa langsung push ke origin master.<br><br>

0b. Setelah di clone, jangan lupa membuat "composer install" atau "update" untuk menginstall file-file yang diperlukan.

1. Settingan Awal .env :
<br>Lakukan create new file .env dan copy semua dari .env.example. Setelah itu lakukan setting seperti dibawah. 
<b>Jangan lupa lakukan "php artisan key:generate"!</b>
<br>
<strong>Gunakan nama DB: "laravel" dan port "3306" <em>(Default)</em></strong>
DB_CONNECTION=mysql <br>
DB_HOST=127.0.0.1 <br>
DB_PORT=3306 <br>
DB_DATABASE=laravel <br>
DB_USERNAME=root <br>
DB_PASSWORD= <br><br>

2. Membuat Databasenya dengan nama laravel. (bisa lewat localhost/phpmyadmin). Setelah itu lakukan "php artisan migrate".
<p>Note : jika akan membuat migrate table baru jangan lupa menggunakan "php artisan make:migration create_namatables_table".</p>

3. Sebelum lakukan perubahan lainnya jangan lupa lakukan Pull jika ada teman yang sudah push ke origin master.

4. Selalu infokan perubahan ke Grup Telegram dan update Readme dibagian Changelog dengan menyertakan Username Gitlab.

5. Jika melakukan push ke branch dan sudah merasa yakin, silahkan lakukan Merge Request dan merge akan approve.

<em>Petunjuk Setelahnya coming soon.</em>
<strong>Jika membutuhkan saran/pertanyaan langsung contact di group Telegram ya.</strong>

<h3>Changelog :</h3>

[20/04/2021 22:57] [ Status : On Development / Finalizing Alpha version ] <em>RaxelAK</em>

+ Mendebug masalah ketika create profile
+ Mengimplementasi One-to-One Eloquent dengan lebih efisien
+ Memperbaiki fitur backend di "follow" dengan menambah query where Eloquent <b>(Vital)</b>

<em> On Progress :</em>

+ Memasang SweetAlert 2 Coming Soon
+ Menambah fitur comment di post dan like comment
+ Fix Show Post Imagenya oversize

<hr>

[19/04/2021 23:56] [ Status : On Development / Finalizing Alpha version ] <em>RaxelAK</em>

+ Menambah fitur like post
+ Memperbaiki tampilan create, show, dan index
+ Mendebug masalah yang ada
+ Memasukan script js dari Sweet Alert (Tinggal dipanggil)

<hr>

[18/04/2021 0:33] [ Status : On Development / Finalizing Alpha version ] <em>RaxelAK</em>

+ Mengubah Profile page dan index page menggunakan container
+ Revisi tombol like dan follow di card post
+ Mendebug masalah yang terjadi di tombol delete profile (tampilannya tidak inline)

<hr>

[17/04/2021 5:40] [ Status : On Development / Finalizing Alpha version ] <em>RaxelAK</em>

+ Mendebug masalah tombol create Profile pada Profile Page
+ Sudah mendukung sistem follow dan unfollow user
+ Membuat profile page menampilkan post yang dibuat oleh user tersebut
+ Membuat migration PostLike -> menambah column status
+ Membuat fondasi migration dan model untuk like dan comment

<em> On Progress :</em>

+ Membuat fitur melihat jumlah following dan followers
+ Membuat fitur search user
+ Revamp profile page design
+ Membentuk ulang fitur like post dan comment
+ Menambah fitur like comment di post
+ Membuat kolom untuk membuat dan melihat komentar di show post
+ Memperbaiki index post menjadi carousel/jumbotron (jika memungkinkan)

<hr>

[17/04/2021 2:47] [ Status : On Development >70%] <em>RaxelAK</em>

+ Merubah tampilan menjadi cards dan menambahkan button likes, comment, dan show post
+ Mendebug masalah pada Profile page
+ Membatasi agar hanya User yang memiliki profile tersebut yang dapat edit dan delete
+ Membatasi agar hanya User yang membuat Post yang dapat edit dan delete
+ Membuat sidebar menjadi fixed sehingga tidak terscroll kebawah

<em>On progress :</em>

+ Memperbaiki agar create profile tidak muncul ke user yang sudah mempunyai profile (selalu bug disini!)
+ Menampilkan tombol follow pada profile user
+ Menampilkan post yang dibuat oleh suatu user di halaman profilenya

<hr>

[16/04/2021 19:09] [ Status : On Development >50%] <em>RaxelAK</em>

+ Menambahkan Create dan Edit Profile dan Debug error yang terjadi ketika edit dan create profile.
+ CRUD untuk post sudah berfungsi (Request pelaporan bug jika masalah terjadi)
+ Register untuk user baru menambahkan user_id di table profile (tahap percobaan)
+ Perbaikan bug pada Register ketika redirect ke HomeController

<em>On progress :</em>

+ Memperbaiki Index supaya menampilkan semua post menggunakan card (Jika waktu cukup)
+ Membuat tombol likes untuk setiap post dan menambahkannya ke database post_likes, tombol berupa icon Font Awesome
+ Membuat tombol komentar untuk setiap post
+ Memperbaiki agar edit profile hanya dapat dilakukan oleh user yang memiliki user_id sesuai
+ Menampilkan tombol follow pada profile user
+ Menampilkan post yang dibuat oleh suatu user di halaman profilenya

<hr>

[15/04/2021] [ Status : On Development] <em>RaxelAK</em>

+ Menambahkan Authentikasi pada Laravel
+ Menambahkan AdminLTE ver 3 sebagai master template
+ Membuat migrations table yang diperlukan
+ Membuat Controller dan Model pada User dan Profile

<em>On progress :</em>

+ FItur Pengecekan Profile berdasarkan User
+ Page membuat/edit profile pribadi
+ Page create post dan memperbaiki Index supaya menampilkan semua post menggunakan card (rencananya)
+ Post yang akan ditampilkan berupa card yang mewajibkan user mengisi judul dan body, serta gambar (opsional).
+ Membuat tombol likes untuk setiap post dan menambahkannya ke database post_likes

[14/04/2021] [ Status : Initial ] <em>RaxelAK</em>

+ edit Readme.md
+ menambah file file dasar initial laravel (tanpa edit)
