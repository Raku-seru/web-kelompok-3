<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $table = "follows";
    protected $fillable = ["fromUserId","toUserId"];

    public function fromuser() {
        return $this->belongsToMany('App\User', 'fromUserId');
    }

    public function touser() {
        return $this->belongsToMany('App\User', 'toUserId');
    }
}
