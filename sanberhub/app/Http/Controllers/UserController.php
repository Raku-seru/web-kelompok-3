<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
use App\Post;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Profile::where('user_id', Auth::id())->get();
        $profilepost = Post::where('user_id', $id)->get();
        $profiledata = Profile::where('user_id', $id)->first();
        return view('adminlte.profile', compact('profile','profilepost','profiledata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminlte.profile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'fullname' => 'required',
    	]);
 
        Profile::create([
    		'fullname' => $request->fullname,
    		'dob' => $request->dob,
            'bio' => $request->bio,
            'user_id' => Auth::id()
    	]);
        
    	return redirect('/user/'.Auth::id())->with('success', 'Profile created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::where('user_id', $id)->get();
        $profilepost = User::find($id)->post;
        $profiledata = User::find($id)->profile;
        $userdata = User::find($id);
        // dd($profile);
        return view('adminlte.profile', compact('profile','profilepost', 'profiledata','userdata'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::where('user_id', $id)->first();
        return view('adminlte.profile.edit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id)
    {
        $this->validate($request,[
    		'fullname' => 'required',
    	]);
        
        $profile = Profile::find($user_id);
        $profile->fullname = $request->fullname;
        $profile->dob = $request->dob;
        $profile->bio = $request->bio;
        $profile->update();
 
    	return redirect('/user/'.Auth::id())->with('success', 'Profile berhasil diedit');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Profile::where('user_id', $id)->first();
        $profile->delete();
        return redirect('/user/'.Auth::id())->with('success', 'Profile berhasil dihapus');
    }
}
