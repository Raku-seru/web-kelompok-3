<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use Auth;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::all();
        return view('adminlte.index',compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminlte.post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'title' => 'required',
    		'body' => 'required',
            'imageurl' => 'mimes:jpeg,png,jpg,JPG,PNG'
    	]);
            
        $gambar = $request->imageurl;
        $name_img = time().' - '.$gambar->getClientOriginalName();

        Post::create([
    		'title' => $request->title,
    		'body' => $request->body,
            'imageurl' => $name_img,
            'caption' => $request->caption,
            'user_id' => Auth::id()
    	]);
 
        $gambar->move('img',$name_img);
    	return redirect('/post')->with('success', 'Post berhasil di buat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('adminlte.post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return view('adminlte.post.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'title' => 'required',
    		'body' => 'required',
            'imageurl' => 'mimes:jpeg,png,jpg,JPG,PNG'
    	]);
            
        $gambar = $request->imageurl;
        $name_img = time().' - '.$gambar->getClientOriginalName();
        
        $post = Post::find($id);
        $post->title = $request->title;
        $post->body = $request->body;
        $post->imageurl = $name_img;
        $post->caption = $request->caption;
        $post->user_id = Auth::id();
        $post->update();
 
        $gambar->move('img',$name_img);
    	return redirect('/post')->with('success', 'Post berhasil di edit.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect('/post');
    }
}
