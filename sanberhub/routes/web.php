<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');;

Route::get('/home', 'HomeController@index')->name('home.alt');

Route::resource('user', 'UserController');

Route::resource('post', 'PostController');

Route::get('/follow/{id}', 'FollowController@following')->name('following');
Route::get('/unfollow/{id}', 'FollowController@unfollow')->name('unfollow');

Route::get('/like/{id}', 'LikeController@liking')->name('liking');
Route::get('/unlike/{id}', 'LikeController@unliking')->name('unliking');