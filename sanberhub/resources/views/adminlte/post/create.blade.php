@extends('adminlte.master')

@section('title')
- Create Post
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="card w-100">
            <div class="card-body bg-primary">
                <h5 class="card-text">Create Post</h5>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-1"></div>
        <div class="col-6">
        <form id="formcreate" name="formcreate" action="/post" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Masukkan Title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Body</label>
                <input type="text" class="form-control" name="body" id="body" placeholder="Masukkan Body">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="imageurl">Image File</label>
                <div class="text-xs text-danger float-right">*Masukkan File Gambar (JPG, JPEG, PNG)</div>
                <input type="file" class="form-control" name="imageurl" id="imageurl" placeholder="Masukkan File Gambar (JPG, JPEG, PNG)">
                @error('imageurl')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="caption">Caption</label>
                <input type="text" class="form-control" name="caption" id="caption" placeholder="Masukkan Caption gambar (jika ada)">
                @error('caption')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button id="create" type="submit" class="btn btn-primary">Post!</button>
        </form>
        </div>
    </div>
</div>
@endsection