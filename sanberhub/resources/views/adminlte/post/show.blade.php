@extends('adminlte.master')

@section('title')
 - Show Post
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="card w-50">
            <div class="card-body bg-primary">
                <h5 class="card-text">Show Post</h5>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-10">
            <div class="card">
            <h5 class="card-header">{{$post->title}}</h5>
            <div class="card-body">
                <h5 class="card-title">Created at : {{$post->created_at}}</h5>
                <p class="card-text">Creator : <a href="/user/{{$post->user_id}}"> {{ $post->user->name }}</p>
                @if ($post->user_id == Auth::id())
                    <span style="display: inline;">
                        <a href="/post/{{$post->id}}/edit" class="btn btn-primary">Edit Post</a>
                        <form action="/post/{{$post->id}}" method="POST" style="display: inline;">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete Post">
                        </form>
                    </span>
                @endif
                <hr>
                <img src="{{asset('img/'.$post->imageurl)}}" alt="{{$post->title}}">
                <p><em>{{$post->caption}}</em></p>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection