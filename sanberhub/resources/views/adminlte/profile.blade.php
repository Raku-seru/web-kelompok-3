    @extends('adminlte.master')

@section('title')
 - Profile {{Auth::user()->name}}
@endsection

@section('header')
<h1>Profile Page</h1>
@endsection

@section('content')

<div class="container">

@if($profiledata == null)
<a href="{{route('user.create')}}" class="btn btn-primary mb-3">Create Profile</a>
@endif



@foreach($profile as $p)
<?php
    $if_null = App\Follow::where('toUserId',$p->user_id)->where('fromUserId',Auth::id())->first();
    
    if(is_null($if_null)){ ?>

        <a href="{{route('following', $p->user_id)}}" class="btn fas fa-heart btn-outline-success mb-3"> Follow </a>

        <?php }
        else { ?>

        <a href="{{route('unfollow', $p->user_id)}}" class="btn btn-outline-success mb-3">Following</a>

        <?php } ?>

@if($p->user_id == Auth::id())
<span style="display: inline;">
    <a href="{{route('user.edit', Auth::id())}}" class="btn fas fa-edit btn-outline-primary mb-3">Edit Profile</a>
    <form action="{{route('user.destroy', Auth::id())}}" method="POST" style="display: inline;">
        @csrf
        @method('DELETE')
        <input type="submit" class="btn btn-outline-danger mb-3" value="Delete Profile">
    </form>
</span>
@endif

<div class="row display-inline">
    <div class="card" style="width: 20%;">
        <div class="card-body">
            <h5 class="card-title"><b>Full name :</b></h5>
            <p class="card-text">{{$p->fullname}}</p>
        </div>
    </div>
    <div class="card" style="width: 20%;">
        <div class="card-body">
            <h5 class="card-title"><b>Date of Birth :</b></h5>
            <p class="card-text">{{$p->dob}}</p>
        </div>
    </div>
    <div class="card" style="width: 60%;">
        <div class="card-body">
            <h5 class="card-title"><b>Biodata :</b></h5>
            <p class="card-text">{{$p->bio}}</p>
        </div>
    </div>
</div>
@endforeach
<h2> {{$userdata->name}} Posts</h2>
<div class="row mx-auto pt-2">
@foreach($profilepost as $pp)
<div class="col-4 mb-3">
    <div class="card border-secondary mb-3" style="width: 22rem;padding-top: 4px;">
        <img class="card-img-top mx-auto" src="/img/{{$pp->imageurl}}" alt="{{$pp->caption}}" style="padding-top:4px;width: 95%;height: auto;">
        <div class="card-header"><b>{{$pp->title}}</b></div>
        <div class="card-body">
            <p class="card-text">{{$pp->body}}</p>
            <a href="#" class="btn btn-light far fa-heart"> Like </a>
            <a href="#" class="btn btn-light far fa-comments"> Comments </a>
            <a href="/post/{{$pp->id}}" class="btn btn-light fas fa-eye"> Show </a>
        </div>
    </div>
</div>
@endforeach
</div>
</div>
@endsection