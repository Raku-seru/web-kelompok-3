@extends('adminlte.master')

@section('title')
 - Create Profile {{Auth::user()->name}}
@endsection

@section('header')
<h1>Create Profile {{Auth::user()->name}} </h1>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="card w-100">
            <div class="card-body bg-primary">
                <h5 class="card-text">Create Profile</h5>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-1"></div>
        <div class="col-6">
            <form action="/user" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
            <label for="fullname">Full Name</label>
            <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Masukkan Nama Lengkap">
            @error('fullname')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
            </div>
            <div class="form-group">
            <label for="dob">Date of Birth</label>
            <input type="date" class="form-control" name="dob" id="dob" placeholder="Masukkan Tanggal Lahir">
            @error('dob')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
            </div>
            <div class="form-group">
            <label for="bio">Biodata</label>
            <input type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan Bio">
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
            </div>
            <button type="submit" class="btn btn-primary">Add Profile</button>
            </form>
        </div>
    </div>
</div>
@endsection