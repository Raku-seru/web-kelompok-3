@extends('adminlte.master')

@section('title')
 - Home
@endsection

@section('header')
<h1>Welcome {{Auth::user()->name}} !</h1>
@endsection

@section('content')
@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif
<div class="container">
    <div class="card w-85">
        <div class="card-body">
            <h5 class="card-title">Hello!</h5>
            <p class="card-text">Apa yang anda ingin buat hari ini?</p>
            <a href="/post/create" class="btn btn-outline-primary mb-3 fas fa-edit">Create Post</a>
        </div>
    </div>
            <div class="row mx-auto pt-2">
                @foreach ($post as $value)
                <div class="col-4 mb-3">
                    <div class="card border-secondary mb-2" style="width: 22rem;padding-top: 2px;">
                        <img class="card-img-top mx-auto d-inline" src="img/{{$value->imageurl}}" alt="{{$value->caption}}" style="padding-top:4px;width: 95%;height: auto;">
                        <div class="card-header"><h5>{{$value->title}}</h5></div>
                            <div class="card-body">
                                <p class="card-text">{{$value->body}}</p>
                                <?php
                                    $if_null = App\PostLike::where('user_id', Auth::id())->where('post_id', $value->id)->first();
                                        if(is_null($if_null)){ ?>
                                            <a href="/like/{{$value->id}}" class="btn btn-light far fa-heart"> Like </a>                                        
                                        <?php }
                                        else { ?>
                                            <a href="/unlike/{{$value->id}}" class="btn btn-light fas fa-heart"> Liked </a>
                                <?php } ?>
                                    <a href="#" class="btn btn-light far fa-comments"> Comments </a>
                                    <a href="post/{{$value->id}}" class="btn btn-light fas fa-eye"> Show </a>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>           
            </div>
</div> 
@endsection